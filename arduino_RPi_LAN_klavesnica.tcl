#!/usr/bin/wish

package require udp

set pismoNadpis "Inconsolata 12 bold"
set pismoTlacid "Inconsolata 10"
set pismoStatus "Inconsolata 8"
set stav "0"
set sockArduino [udp_open]
set sockHMI [udp_open]
set IP_HMI "192.168.1.104"
set IP_ARDUINO "192.168.1.105"
set PORT_HMI "8889"
set PORT_ARDUINO "8888"
set statusString "HMI: ${IP_HMI} @ ${PORT_HMI} | ARDUINO: ${IP_ARDUINO} @ ${PORT_ARDUINO}"
set arduinoMillis "MILLIS will be here!"
set prikazKlavesnice "onboard &"
set timeoutKillKeyboard 10000

set srv [udp_open $PORT_HMI] 
fconfigure $srv -buffering none
fileevent $srv readable [list udpEventHandler $srv]

proc udpOtvor {} {
    global sockArduino sockHMI IP_HMI IP_ARDUINO PORT_HMI PORT_ARDUINO
    fconfigure $sockArduino -buffering none -translation binary -remote [list $IP_ARDUINO $PORT_ARDUINO]
    puts "Settings: [fconfigure $sockArduino]"
}

proc udpEventHandler {sock} {
    global arduinoMillis
    set sprava [read $sock]
    puts "Received from Arduino: $sprava"
    set arduinoMillis $sprava
    return
}

proc outHigh {} {
    global stav sockArduino srv
    set stav "1" 
    puts  $sockArduino $stav
    flush $sockArduino  
}

proc outBlink {} {
    global stav sockArduino pauza .btnInterval
    outHigh
    .btnInterval configure -background "gray"
    after [expr {$pauza * 1000}] {
	.btnInterval configure -background "lightgreen"
	outLow
    }
}

proc outLow {} {
    global stav sockArduino
    set stav "0" 
    puts  $sockArduino $stav
    flush $sockArduino
}

proc killProcessKeyboard {} {
    global prikazKlavesnice
    set prikazKill "killall "
    append spojene $prikazKill $prikazKlavesnice
    exec {*}$spojene
}

proc onSpinBoxClick {} {
    global prikazKlavesnice timeoutKillKeyboard
    set prikazGrep "ps ax | grep -c "
    append spojene $prikazGrep $prikazKlavesnice
    set psOutput [exec {*}[string replace $spojene end-1 end ""]]
    if {$psOutput ne "1"} {
	puts "Program už beží!"
    } else {
	puts "Program ešte nebeží!"
	exec {*}$prikazKlavesnice
	after $timeoutKillKeyboard killProcessKeyboard
    }
}

proc quitProgramm {} {
    global sockArduino sockHMI
    close $sockArduino
    close $sockHMI
    puts "Closing connection."
    exit
}

proc dizajn {} {
    global pismoNadpis pismoTlacid stav pismoStatus IP_HMI IP_ARDUINO PORT_HMI PORT_ARDUINO statusString arduinoMillis
    set btnIntervalFarba "lightgreen"
    label .lblNadpis -text  "Test RPi (HMI) -> Arduino MEGA" -font $pismoNadpis
    frame .boxes
    button .btnStart     -text  " OUTPUT HIGH " -font $pismoTlacid -bg green -command outHigh
    button .btnStop      -text  " OUTPUT LOW  " -font $pismoTlacid  -bg red1 -command outLow
    button .btnInterval  -text  " OUTPUT BLINK" -font $pismoTlacid  -bg $btnIntervalFarba -command outBlink
    button .btnKoniec  -text  "QUIT" -font $pismoTlacid  -command quitProgramm
    label .lblTextStav -text "STATE" -font $pismoTlacid 
    label .lblStavHlav -textvariable stav  -font $pismoTlacid
    label .lblTextInterval -text "INTERVAL" -font $pismoTlacid 
    spinbox .spinInterval -from 1 -to 30 -textvariable pauza -width 2
    label .lblMillis -textvariable arduinoMillis -font $pismoTlacid
    label .lblIPHMI -textvariable statusString -font $pismoStatus

    grid .lblNadpis -column 1 -row 0 
    grid .btnStart -sticky n -column 1 -row 3 -pady 10 -padx 10
    grid .btnInterval -sticky n -column 1 -row 4 -pady 10 -padx 10
    grid .btnStop -sticky n -column 1 -row 5  -pady 10 -padx 10
    grid .lblMillis -sticky n -column 1 -row 6
    grid .lblTextStav -sticky n -column 2  -row 3 -pady 10 -padx 10
    grid .lblStavHlav -sticky n -column 2  -row 4 -pady 10 -padx 10
    grid .lblTextInterval -sticky n -column 3 -row 3 -pady 10 -padx 10
    grid .spinInterval -sticky n -column 3 -row 4 -pady 10 -padx 10
    grid .btnKoniec -sticky n -column 3 -row 6  -pady 10 -padx 10
    grid .lblIPHMI -column 1 -row 7

    bind .spinInterval <Button-1> {onSpinBoxClick}
}


## main ##

dizajn 
udpOtvor
