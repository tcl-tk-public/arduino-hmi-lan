# Controlling Arduino from HMI (incl. Raspberry Pi)



## Overview

Here are two archives. One, **arduino_LAN.tar.gz** is a firmware for *Arduino Mega* + *Wiznet 5100*, for receiving and sending UDP packets.

Second, **arduino_RPi_LAN.tar.gz** is a *Tcl/Tk* file for HMI touch screen, running together with Raspberry Pi.

The blog about this combo is here: [Arduino a Tcl/Tk - časť 2: HMI cez LAN](https://linuxos.sk/blog/richard/detail/arduino-a-tcltk-cast-2-hmi-cez-lan/)

Edit 22/11/2023: Added virtual keyboard for entering the values into spinbox: [Arduino a Tcl/Tk, časť 3 - HMI | klávesnica](https://linuxos.sk/blog/richard/detail/arduino-a-tcltk-cast-3-hmi-klavesnica/)
